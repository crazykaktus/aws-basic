#!/bin/bash
# use for local usage and testing
ansible-playbook ./ansible/install-docker.yml -i ./ansible/inventory
ansible-playbook ./ansible/copy-docker-compose.yml -i ./ansible/inventory
ansible-playbook ./ansible/docker-compose-run.yml -i ./ansible/inventory

